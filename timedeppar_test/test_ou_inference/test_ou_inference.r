# ===============================================
# test inference of Ornstein-Uhlenbeck parameters
# ===============================================


# global parameters:
# ==================

test.tasks           <- T

theta                <- 1
sd.create            <- 0.2
sd.prior             <- 0.2
gamma.create         <- 10
gamma.infer          <- 1
obserr               <- 0.05
t.states             <- seq(from=0,to=2,length.out=1001)
ints.obs             <- c(1,10)
log.ou               <- FALSE

n.iter               <- 5000
n.interval           <- 50
thin                 <- 10
#splitmethod          <- "modunif"
#splitmethod          <- "random"
splitmethod          <- "autoweights"
n.const.perstep      <- 1

verbose              <- 0

fract.t              <- 0.25        # fraction of time axis used for 2nd diagnostics plot
plot.width           <-  8.27
plot.height          <- 11.69


labels               <- expression(theta       = theta,
                                   obserr      = epsilon,
                                   theta_mean  = mu[theta],
                                   theta_sd    = sigma[theta],
                                   theta_gamma = gamma[theta])


# library:
# ========

library(timedeppar)


# generate data:
# ==============

set.seed(123)
data.states  <- randOU(mean=theta,sd=sd.create,gamma=gamma.create,t=t.states,log=log.ou) 
# first create obs everywhere and select later to have the same observations for differnt observation intervals:
data.obs.all <- data.states; data.obs.all[,2] <- data.obs.all[,2]+rnorm(nrow(data.states),mean=0,sd=obserr)
colnames(data.obs.all)[2] <- "yobs"

for ( int.obs in ints.obs )
{
  # select observations according to observation intervals:
  data.obs     <- data.obs.all[seq(from=1,to=length(t.states),by=int.obs),]
  t.obs        <- data.obs[,1]
  pdf(paste("test_ou_inference",int.obs,"0.pdf",sep="_"),width=plot.width,height=plot.height)
  xlim <- range(t.states)
  # choose plot range independent of observation interval:
  ylim <- theta + max(abs(c(data.states[,2],data.obs.all[,2])-theta))*c(-1,1)
  plot(data.states,type="l",main="true process and simulated observations",
       xaxs="i",yaxs="i",xlab="t",ylab=get.label("theta",labels),xlim=xlim,ylim=ylim)
  points(data.obs,pch=19,cex=0.5)
  abline(h=theta)
  abline(h=theta-sd.create,lty="dashed")
  abline(h=theta+sd.create,lty="dashed")
  abline(h=theta-1.96*sd.create,lty="dotted")
  abline(h=theta+1.96*sd.create,lty="dotted")
  dyrel <- 0.017
  x1 <- xlim[1]+0.05*diff(xlim)
  y1 <- ylim[2]-dyrel*diff(ylim)
  text(x=x1,y=y1,labels=paste("mean =",theta),adj=0)
  text(x=x1,y=y1-dyrel*diff(ylim),labels=paste("sd =",sd.create),adj=0)
  text(x=x1,y=y1-2*dyrel*diff(ylim),labels=paste("gamma =",gamma.create),adj=0)
  text(x=x1,y=y1-3*dyrel*diff(ylim),labels=paste("obserr =",obserr),adj=0)
  dev.off()
  
  
  # define likelihood:
  # ==================
  
  loglikeli <- function(param,param.default=list(theta=0,obserr=1),data)
  {
    # construct local parameter list combining actual with default parameters:
    param.loc <- param.default; for ( i in 1:length(param) ) param.loc[[names(param)[i]]] <- param[[i]]
    
    # check completeness of parameter list:
    if ( ! "theta"  %in% names(param.loc) ) stop("*** parameter \"theta\" is missing")
    if ( ! "obserr" %in% names(param.loc) ) stop("*** parameter \"obserr\" is missing")
    if ( is.matrix(param.loc$obserr) )      stop("*** the observation error is not allowed to be time-dependent")
    
    # construct time series:
    theta <- param$theta
    if ( is.matrix(theta) | is.data.frame(theta) ) theta <- approx(x=param$theta[,1],y=param$theta[,2],xout=data[,1])$y
    
    # calculate likelihood:
    loglikeli <- sum(dnorm(data[,"yobs"],mean=theta,sd=param.loc$obserr,log=TRUE))
    
    # return result:
    return(loglikeli)
  }
  
  
  # infer parameters using uniform prior:
  # =====================================
  
  param.default <- list(theta=theta,obserr=obserr)
  
  
  # 1. Infer constant parameter theta given obserr:
  # -----------------------------------------------
  
  # starting from scratch:
  
  cat("\n1: Infer constant parameter theta:")
  cat("\n----------------------------------\n")
  res1 <- infer.timedeppar(loglikeli     = loglikeli,
                           param.ini     = param.default["theta"],
                           param.log     = c(theta=log.ou),
                           param.range   = list(theta=c(0,100)),
                           n.iter        = n.iter,
                           control       = list(thin=thin,
                                                n.const.perstep=n.const.perstep),
                           verbose       = verbose,
                           file.save     = paste("test_ou_inference",int.obs,"1",sep="_"),
                           param.default = param.default,
                           data          = data.obs)
  pdf(paste("test_ou_inference",int.obs,"1.pdf",sep="_"),width=plot.width,height=plot.height)
  plot(res1,labels=labels,n.burnin=0.5*n.iter)
  dev.off()
  
  if ( test.tasks )
  {
    # extending previous Markov chain:
    
    res1a <- infer.timedeppar(task                 = "continue",
                              res.infer.timedeppar = res1,
                              n.iter               = 3*n.iter,
                              verbose              = verbose,
                              file.save            = paste("test_ou_inference",int.obs,"1_continuation",sep="_"),
                              param.default        = param.default,
                              data                 = data.obs)
    pdf(paste("test_ou_inference",int.obs,"1_continuation.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res1a,labels=labels,n.burnin=n.iter)
    dev.off()
    
    # restarting from previously found end of previous chain:
    
    res1b <- infer.timedeppar(task                 = "restart",
                              res.infer.timedeppar = res1a,
                              n.iter               = 2*n.iter,
                              control              = list(n.adapt.cov=0),
                              verbose              = verbose,
                              file.save            = paste("test_ou_inference",int.obs,"1_restart",sep="_"),
                              param.default        = param.default,
                              data                 = data.obs)
    pdf(paste("test_ou_inference",int.obs,"1_restart.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res1b,labels=labels,n.burnin=n.iter)
    dev.off()
  }
  
  
  # 2. Infer constant parameters theta and obserr:
  # ----------------------------------------------
  
  # starting from scratch:
  
  cat("\n2: Infer constant parameters theta and obserr:")
  cat("\n----------------------------------------------\n")
  res2 <- infer.timedeppar(loglikeli     = loglikeli,
                           param.ini     = param.default[c("theta","obserr")],
                           param.range   = list(obserr=c(0,100)),
                           param.log     = c(obserr=TRUE),
                           n.iter        = n.iter,
                           control       = list(thin=thin,
                                                n.const.perstep=n.const.perstep),
                           verbose       = verbose,
                           file.save     = paste("test_ou_inference",int.obs,"2",sep="_"),
                           param.default = param.default,
                           data          = data.obs)
  pdf(paste("test_ou_inference",int.obs,"2.pdf",sep="_"),width=plot.width,height=plot.height)
  plot(res2,labels=labels,n.burnin=0.5*n.iter)
  dev.off()
  
  if ( test.tasks )
  {
    # extending previous Markov chain:
    
    res2a <- infer.timedeppar(task                 = "continue",
                              res.infer.timedeppar = res2,
                              n.iter               = 3*n.iter,
                              verbose              = verbose,
                              file.save            = paste("test_ou_inference",int.obs,"2_continuation",sep="_"),
                              param.default        = param.default,
                              data                 = data.obs)
    pdf(paste("test_ou_inference",int.obs,"2_continuation.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res2a,labels=labels,n.burnin=n.iter)
    dev.off()
    
    # restarting from previously found end of previous chain:
    
    res2b <- infer.timedeppar(task                 = "restart",
                              res.infer.timedeppar = res2a,
                              n.iter               = 2*n.iter,
                              control              = list(n.adapt.cov=0),
                              verbose              = verbose,
                              file.save            = paste("test_ou_inference",int.obs,"2_restart",sep="_"),
                              param.default        = param.default,
                              data                 = data.obs)
    pdf(paste("test_ou_inference",int.obs,"2_restart.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res2b,labels=labels,n.burnin=n.iter)
    dev.off()
  }
  
  
  # 3. Infer time-dependent parameter theta given gamma and obserr:
  # ---------------------------------------------------------------
  
  # starting from scratch:
  
  cat("\n3: Infer time-dependent parameter theta:")
  cat("\n----------------------------------------\n")
  res3 <- infer.timedeppar(loglikeli         = loglikeli,
                           param.ini         = list(theta=randOU(mean=theta,sd=sd.prior,gamma=gamma.infer,t=t.states,log=log.ou)),
                           param.ou.ini      = c(theta_mean=theta,theta_sd=sd.prior),
                           param.ou.fixed    = c(theta_gamma=gamma.infer),
                           param.ou.logprior = function(x) { if ( "theta_sd" %in% names(x) ) return (2*dnorm(x["theta_sd"],0,0.2)) else return(0) },
                           n.iter            = n.iter,
                           control           = list(n.interval      = n.interval,
                                                    thin            = thin,
                                                    splitmethod     = splitmethod,
                                                    n.const.perstep = n.const.perstep,
                                                    save.diag       = TRUE),
                           verbose           = verbose,
                           file.save         = paste("test_ou_inference",int.obs,"3",sep="_"),
                           param.default     = param.default,
                           data              = data.obs)
  pdf(paste("test_ou_inference",int.obs,"3.pdf",sep="_"),width=plot.width,height=plot.height)
  plot(res3,labels=labels,n.burnin=0.5*n.iter,nrow.timedeppar=2)
  dev.off()
  pdf(paste("test_ou_inference",int.obs,"3_diagnostics.pdf",sep="_"),width=plot.width,height=plot.height)
  plot(res3,type="diagnostics",labels=labels,n.burnin=0.5*n.iter,nrow.timedeppar=2)
  plot(res3,type="diagnostics",labels=labels,n.burnin=0.5*n.iter,nrow.timedeppar=2,
       xlim.ts=min(t.states)+c(0,fract.t)*(max(t.states)-min(t.states)))
  dev.off()
  
  if ( test.tasks )
  {
    # extending previous Markov chain:
    
    res3a <- infer.timedeppar(task                 = "continue",
                              res.infer.timedeppar = res3,
                              n.iter               = 3*n.iter,
                              verbose              = verbose,
                              file.save            = paste("test_ou_inference",int.obs,"3_continuation",sep="_"),
                              param.default        = param.default,
                              data                 = data.obs)
    pdf(paste("test_ou_inference",int.obs,"3_continuation.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res3a,labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    dev.off()
    pdf(paste("test_ou_inference",int.obs,"3_continuation_diagnostics.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res3a,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    plot(res3a,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2,
         xlim.ts=min(t.states)+c(0,fract.t)*(max(t.states)-min(t.states)))
    dev.off()
    
    # restarting from previously found end of previous chain:
    
    res3b <- infer.timedeppar(task                 = "restart",
                              res.infer.timedeppar = res3a,
                              n.iter               = 2*n.iter,
                              control              = list(n.adapt.cov=0),
                              verbose              = verbose,
                              file.save            = paste("test_ou_inference",int.obs,"3_restart",sep="_"),
                              param.default        = param.default,
                              data                 = data.obs)
    pdf(paste("test_ou_inference",int.obs,"3_restart.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res3b,labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    dev.off()
    pdf(paste("test_ou_inference",int.obs,"3_restart_diagnostics.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res3b,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    plot(res3b,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2,
         xlim.ts=min(t.states)+c(0,fract.t)*(max(t.states)-min(t.states)))
    dev.off()
  }
  
  
  # 4. Infer time-dependent parameter theta and obserr given gamma:
  # ---------------------------------------------------------------
  
  # starting from scratch:
  
  cat("\n4: Infer time-dependent parameter theta and constant observation error:")
  cat("\n-----------------------------------------------------------------------\n")
  res4 <- infer.timedeppar(loglikeli         = loglikeli,
                           param.ini         = list(theta=randOU(mean=theta,sd=sd.prior,gamma=gamma.infer,t=t.states,log=log.ou),obserr=obserr),
                           param.log         = c(obserr=TRUE),
                           param.range       = list(obserr=c(0,100)),
                           param.ou.ini      = c(theta_mean=theta,theta_sd=sd.prior),
                           param.ou.fixed    = c(theta_gamma=gamma.infer),
                           param.ou.logprior = function(x) { if ( "theta_sd" %in% names(x) ) return (2*dnorm(x["theta_sd"],0,0.2)) else return(0) },
                           n.iter            = n.iter,
                           control           = list(n.interval      = n.interval,
                                                    thin            = thin,
                                                    splitmethod     = splitmethod,
                                                    n.const.perstep = n.const.perstep,
                                                    save.diag       = TRUE),
                           verbose           = verbose,
                           file.save         = paste("test_ou_inference",int.obs,"4",sep="_"),
                           param.default     = param.default,
                           data              = data.obs)
  pdf(paste("test_ou_inference",int.obs,"4.pdf",sep="_"),width=plot.width,height=plot.height)
  plot(res4,labels=labels,n.burnin=0.5*n.iter,nrow.timedeppar=2)
  dev.off()
  pdf(paste("test_ou_inference",int.obs,"4_diagnostics.pdf",sep="_"),width=plot.width,height=plot.height)
  plot(res4,type="diagnostics",labels=labels,n.burnin=0.5*n.iter,nrow.timedeppar=2)
  plot(res4,type="diagnostics",labels=labels,n.burnin=0.5*n.iter,nrow.timedeppar=2,
       xlim.ts=min(t.states)+c(0,fract.t)*(max(t.states)-min(t.states)))
  dev.off()
  
  if ( test.tasks )
  {
    # extending previous Markov chain:
    
    res4a <- infer.timedeppar(task                 = "continue",
                              res.infer.timedeppar = res4,
                              n.iter               = 3*n.iter,
                              verbose              = verbose,
                              file.save            = paste("test_ou_inference",int.obs,"4_continuation",sep="_"),
                              param.default        = param.default,
                              data                 = data.obs)
    pdf(paste("test_ou_inference",int.obs,"4_continuation.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res4a,labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    dev.off()
    pdf(paste("test_ou_inference",int.obs,"4_continuation_diagnostics.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res4a,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    plot(res4a,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2,
         xlim.ts=min(t.states)+c(0,fract.t)*(max(t.states)-min(t.states)))
    dev.off()
    
    # restarting from previously found end of previous chain:
    
    res4b <- infer.timedeppar(task                 = "restart",
                              res.infer.timedeppar = res4a,
                              n.iter               = 2*n.iter,
                              control              = list(n.adapt.cov=0),
                              verbose              = verbose,
                              file.save            = paste("test_ou_inference",int.obs,"4_restart",sep="_"),
                              param.default        = param.default,
                              data                 = data.obs)
    pdf(paste("test_ou_inference",int.obs,"4_restart.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res4b,labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    dev.off()
    pdf(paste("test_ou_inference",int.obs,"4_restart_diagnostics.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res4b,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    plot(res4b,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2,
         xlim.ts=min(t.states)+c(0,fract.t)*(max(t.states)-min(t.states)))
    dev.off()
  }
  
  
  # 5. Infer time-dependent parameter theta given obserr:
  # -----------------------------------------------------
  
  # starting from scratch:
  
  cat("\n5: Infer time-dependent parameter theta with given observation error:")
  cat("\n-------------------------------------------------------------------\n")
  res5 <- infer.timedeppar(loglikeli         = loglikeli,
                           param.ini         = list(theta=randOU(mean=theta,sd=sd.prior,gamma=gamma.infer,t=t.states,log=log.ou)),
                           param.ou.ini      = c(theta_mean=theta,theta_sd=sd.prior,theta_gamma=gamma.infer),
                           param.ou.logprior = function(x) { if ( "theta_sd" %in% names(x) ) return (2*dnorm(x["theta_sd"],0,0.2)) else return(0) },
                           n.iter            = n.iter,
                           control           = list(n.interval      = n.interval,
                                                    thin            = thin,
                                                    splitmethod     = splitmethod,
                                                    n.const.perstep = n.const.perstep,
                                                    save.diag       = TRUE),
                           verbose           = verbose,
                           file.save         = paste("test_ou_inference",int.obs,"5",sep="_"),
                           param.default     = param.default,
                           data              = data.obs)
  pdf(paste("test_ou_inference",int.obs,"5.pdf",sep="_"),width=plot.width,height=plot.height)
  plot(res5,labels=labels,n.burnin=0.5*n.iter,nrow.timedeppar=2)
  dev.off()
  pdf(paste("test_ou_inference",int.obs,"5_diagnostics.pdf",sep="_"),width=plot.width,height=plot.height)
  plot(res5,type="diagnostics",labels=labels,n.burnin=0.5*n.iter,nrow.timedeppar=2)
  plot(res5,type="diagnostics",labels=labels,n.burnin=0.5*n.iter,nrow.timedeppar=2,
       xlim.ts=min(t.states)+c(0,fract.t)*(max(t.states)-min(t.states)))
  dev.off()
  
  if ( test.tasks )
  {
    # extending previous Markov chain:
    
    res5a <- infer.timedeppar(task                 = "continue",
                              res.infer.timedeppar = res5,
                              n.iter               = 3*n.iter,
                              verbose              = verbose,
                              file.save            = paste("test_ou_inference",int.obs,"5_continuation",sep="_"),
                              param.default        = param.default,
                              data                 = data.obs)
    pdf(paste("test_ou_inference",int.obs,"5_continuation.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res5a,labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    dev.off()
    pdf(paste("test_ou_inference",int.obs,"5_continuation_diagnostics.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res5a,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    plot(res5a,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2,
         xlim.ts=min(t.states)+c(0,fract.t)*(max(t.states)-min(t.states)))
    dev.off()
    
    # restarting from previously found end of previous chain:
    
    res5b <- infer.timedeppar(task                 = "restart",
                              res.infer.timedeppar = res5a,
                              n.iter               = 2*n.iter,
                              control              = list(n.adapt.cov=0),
                              verbose              = verbose,
                              file.save            = paste("test_ou_inference",int.obs,"5_restart",sep="_"),
                              param.default        = param.default,
                              data                 = data.obs)
    pdf(paste("test_ou_inference",int.obs,"5_restart.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res5b,labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    dev.off()
    pdf(paste("test_ou_inference",int.obs,"5_restart_diagnostics.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res5b,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    plot(res5b,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2,
         xlim.ts=min(t.states)+c(0,fract.t)*(max(t.states)-min(t.states)))
    dev.off()
  }
  
  
  # 6. Infer time-dependent parameter theta and obserr:
  # ---------------------------------------------------
  
  # starting from scratch:
  
  cat("\n6: Infer time-dependent parameter theta and constant observation error:")
  cat("\n---------------------------------------------------------------------\n")
  res6 <- infer.timedeppar(loglikeli         = loglikeli,
                           param.ini         = list(theta=randOU(mean=theta,sd=sd.prior,gamma=gamma.infer,t=t.states,log=log.ou),obserr=obserr),
                           param.log         = c(obserr=TRUE),
                           param.range       = list(obserr=c(0,100)),
                           param.ou.ini      = c(theta_mean=theta,theta_sd=sd.prior,theta_gamma=gamma.infer),
                           param.ou.logprior = function(x) { if ( "theta_sd" %in% names(x) ) return (2*dnorm(x["theta_sd"],0,0.2)) else return(0) },
                           n.iter            = n.iter,
                           control           = list(n.interval      = n.interval,
                                                    thin            = thin,
                                                    splitmethod     = splitmethod,
                                                    n.const.perstep = n.const.perstep,
                                                    save.diag       = TRUE),
                           verbose           = verbose,
                           file.save         = paste("test_ou_inference",int.obs,"6",sep="_"),
                           param.default     = param.default,
                           data              = data.obs)
  pdf(paste("test_ou_inference",int.obs,"6.pdf",sep="_"),width=plot.width,height=plot.height)
  plot(res6,labels=labels,n.burnin=0.5*n.iter,nrow.timedeppar=2)
  dev.off()
  pdf(paste("test_ou_inference",int.obs,"6_diagnostics.pdf",sep="_"),width=plot.width,height=plot.height)
  plot(res6,type="diagnostics",labels=labels,n.burnin=0.5*n.iter,nrow.timedeppar=2)
  plot(res6,type="diagnostics",labels=labels,n.burnin=0.5*n.iter,nrow.timedeppar=2,
       xlim.ts=min(t.states)+c(0,fract.t)*(max(t.states)-min(t.states)))
  dev.off()
  
  if ( test.tasks )
  {
    # extending previous Markov chain:
    
    res6a <- infer.timedeppar(task                 = "continue",
                              res.infer.timedeppar = res6,
                              n.iter               = 3*n.iter,
                              verbose              = verbose,
                              file.save            = paste("test_ou_inference",int.obs,"6_continuation",sep="_"),
                              param.default        = param.default,
                              data                 = data.obs)
    pdf(paste("test_ou_inference",int.obs,"6_continuation.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res6a,labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    dev.off()
    pdf(paste("test_ou_inference",int.obs,"6_continuation_diagnostics.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res6a,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    plot(res6a,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2,
         xlim.ts=min(t.states)+c(0,fract.t)*(max(t.states)-min(t.states)))
    dev.off()
    
    # restarting from previously found end of previous chain:
    
    res6b <- infer.timedeppar(task                 = "restart",
                              res.infer.timedeppar = res6a,
                              n.iter               = 2*n.iter,
                              control              = list(n.adapt.cov=0),
                              verbose              = verbose,
                              file.save            = paste("test_ou_inference",int.obs,"6_restart",sep="_"),
                              param.default        = param.default,
                              data                 = data.obs)
    pdf(paste("test_ou_inference",int.obs,"6_restart.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res6b,labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    dev.off()
    pdf(paste("test_ou_inference",int.obs,"6_restart_diagnostics.pdf",sep="_"),width=plot.width,height=plot.height)
    plot(res6b,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2)
    plot(res6b,type="diagnostics",labels=labels,n.burnin=n.iter,nrow.timedeppar=2,
         xlim.ts=min(t.states)+c(0,fract.t)*(max(t.states)-min(t.states)))
    dev.off()
  }
  
  
  pdf(paste("test_ou_inference",int.obs,"6_continuation_analysis_sigma_gamma.pdf",sep="_"),height=6,width=12)
  par(mfrow=c(1,2))
  ind <- ceiling(0.25*nrow(res6a$sample.logpdf)):nrow(res6a$sample.logpdf)
  plot(res6a$sample.param.ou[ind,"theta_gamma"],
       res6a$sample.param.ou[ind,"theta_sd"],
       xlab=expression(gamma[theta]),ylab=expression(sigma[theta]),pch=19,cex=0.5)
  plot(res6a$sample.param.ou[ind,"theta_gamma"],
       sqrt(2*res6a$sample.param.ou[ind,"theta_gamma"])*res6a$sample.param.ou[ind,"theta_sd"],
       xlab=expression(gamma[theta]),ylab=expression(sqrt(2*gamma[theta])*sigma[theta]),pch=19,cex=0.5)
  dev.off()
  
  
  pdf(paste("test_ou_inference",int.obs,"6_continuation_maxpost.pdf",sep="_"),width=plot.width,height=plot.height)
  
  xlim <- range(t.states)
  ylim <- theta + max(abs(c(data.states[,2],data.obs.all[,2])-theta))*c(-1,1)
  plot(data.states,type="l",main="true process and simulated observations",
       xaxs="i",yaxs="i",xlab="t",ylab=expression(theta),xlim=xlim,ylim=ylim)
  points(data.obs,pch=19,cex=0.5)
  abline(h=theta)
  dyrel <- 0.017
  x1 <- xlim[1]+0.05*diff(xlim)
  x2 <- xlim[1]+0.30*diff(xlim)
  x3 <- xlim[1]+0.55*diff(xlim)
  y1 <- ylim[2]-dyrel*diff(ylim)
  text(x=x1,y=y1                   ,labels="true par.:",adj=0)
  text(x=x1,y=y1-  dyrel*diff(ylim),labels=paste("mean =",theta),adj=0)
  text(x=x1,y=y1-2*dyrel*diff(ylim),labels=paste("sd =",sd.create),adj=0)
  text(x=x1,y=y1-3*dyrel*diff(ylim),labels=paste("gamma =",gamma.create),adj=0)
  text(x=x1,y=y1-4*dyrel*diff(ylim),labels=paste("obserr =",obserr),adj=0)
  
  param.maxpost <- get.param(res6a)
  sample.logpdf <- res6a$sample.logpdf[,"logposterior"]
  lines(param.maxpost$param$theta,col="blue")
  text(x=x2,y=y1                   ,labels="max. post.:",adj=0,col="blue")
  text(x=x2,y=y1-  dyrel*diff(ylim),labels=paste("mean =",round(param.maxpost$param.ou.estim["theta_mean"],2)),adj=0,col="blue")
  text(x=x2,y=y1-2*dyrel*diff(ylim),labels=paste("sd =",round(param.maxpost$param.ou.estim["theta_sd"],3)),adj=0,col="blue")
  text(x=x2,y=y1-3*dyrel*diff(ylim),labels=paste("gamma =",round(param.maxpost$param.ou.estim["theta_gamma"],2)),adj=0,col="blue")
  text(x=x2,y=y1-4*dyrel*diff(ylim),labels=paste("obserr =",round(param.maxpost$param[["obserr"]],2)),adj=0,col="blue")
  text(x=x2,y=y1-5*dyrel*diff(ylim),labels=paste("logpost =",round(param.maxpost$logpdf["logposterior"],1)),adj=0,col="blue")
  text(x=x2,y=y1-6*dyrel*diff(ylim),labels=paste("loglikeli =",round(param.maxpost$logpdf["loglikeliobs"],1)),adj=0,col="blue")
  text(x=x2,y=y1-7*dyrel*diff(ylim),labels=paste("logpdfou =",round(param.maxpost$logpdf["logpdfou_timedeppar_theta"],1)),adj=0,col="blue")
  text(x=x2,y=y1-8*dyrel*diff(ylim),labels=paste("rmse_true =",round(sqrt(mean((approx(param.maxpost$param$theta,xout=t.obs)$y-data.obs[,2])^2)),4)),adj=0,col="blue")
  text(x=x2,y=y1-9*dyrel*diff(ylim),labels=paste("index =",param.maxpost$ind.chain),adj=0,col="blue")
  
  ind.maxlast   <- ceiling(0.5*length(sample.logpdf))-1+which.max(sample.logpdf[ceiling(0.5*length(sample.logpdf)):length(sample.logpdf)])
  param.maxlast <- get.param(res6a,ind.maxlast)
  lines(param.maxlast$param$theta,col="red")
  text(x=x3,y=y1                   ,labels="max. 2nd half:",adj=0,col="red")
  text(x=x3,y=y1-  dyrel*diff(ylim),labels=paste("mean =",round(param.maxlast$param.ou.estim["theta_mean"],2)),adj=0,col="red")
  text(x=x3,y=y1-2*dyrel*diff(ylim),labels=paste("sd =",round(param.maxlast$param.ou.estim["theta_sd"],3)),adj=0,col="red")
  text(x=x3,y=y1-3*dyrel*diff(ylim),labels=paste("gamma =",round(param.maxlast$param.ou.estim["theta_gamma"],2)),adj=0,col="red")
  text(x=x3,y=y1-4*dyrel*diff(ylim),labels=paste("obserr =",round(param.maxlast$param[["obserr"]],2)),adj=0,col="red")
  text(x=x3,y=y1-5*dyrel*diff(ylim),labels=paste("logpost =",round(param.maxlast$logpdf["logposterior"],1)),adj=0,col="red")
  text(x=x3,y=y1-6*dyrel*diff(ylim),labels=paste("loglikeli =",round(param.maxlast$logpdf["loglikeliobs"],1)),adj=0,col="red")
  text(x=x3,y=y1-7*dyrel*diff(ylim),labels=paste("logpdfou =",round(param.maxlast$logpdf["logpdfou_timedeppar_theta"],1)),adj=0,col="red")
  text(x=x3,y=y1-8*dyrel*diff(ylim),labels=paste("rmse_true =",round(sqrt(mean((approx(param.maxlast$param$theta,xout=t.obs)$y-data.obs[,2])^2)),4)),adj=0,col="red")
  text(x=x3,y=y1-9*dyrel*diff(ylim),labels=paste("index =",param.maxlast$ind.chain),adj=0,col="red")
  
  dev.off()
  
  logpdfOU(t     = param.maxlast$param$theta[,1],
           y     = param.maxlast$param$theta[,2],
           mean  = param.maxlast$param.ou.estim["theta_mean"],
           sd    = param.maxlast$param.ou.estim["theta_sd"],
           gamma = param.maxlast$param.ou.estim["theta_gamma"])
  
}


