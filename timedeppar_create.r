#################################
# create package
#################################


# write documentation:
if ( !require("roxygen2") ) { install.packages("roxygen2"); library("roxygen2") }
roxygenize("timedeppar")

# check package:
system("R CMD check timedeppar")

# build source package (.tar.gz):
system("R CMD build timedeppar")
file.copy("./timedeppar.Rcheck/timedeppar-manual.pdf","timedeppar-manual.pdf",overwrite=TRUE)

# check processed package:
system("R CMD check --as-cran timedeppar_1.0.3.tar.gz")

detach("package:timedeppar", unload = TRUE)
install.packages("timedeppar_1.0.3.tar.gz",repos=NULL,type="source")
library(timedeppar)
help(timedeppar)

# upload:
# https://cran.r-project.org/submit.html

